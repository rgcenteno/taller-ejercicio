/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.cs1daw.tallerprograma;

import com.mitaller.entidades.*;
import java.time.LocalDate;
import java.time.Month;
/**
 *
 * @author rgcenteno
 */
public class TallerPrograma {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {       
        Taller taller = new Taller("Mecánica del motor S. L.");
        
        Mecanico m1 = new Mecanico("M345698K", "33345565G", "Juan Pérez", 1500);
        Mecanico m2 = new Mecanico("M456123K", "32654123B", "Ángela Estévez", 1600);
        Mecanico m3 = new Mecanico("M876123M", "24351981H", "Adri Velasco", 1400);
        
        Coche c1 = new Coche("3464JVM", "Seat", "ibiza", 120, LocalDate.of(2007, Month.APRIL, 12));
        Coche c2 = new Coche("8761MKJ", "Opel", "Astra", 150, LocalDate.of(2014, Month.SEPTEMBER, 15));
        Coche c3 = new Coche("1234CBM", "Ford", "Mondeo", 100, LocalDate.of(2001, Month.MAY, 3));                
                
        c1.viajar(100);
        c1.viajar(200);
        c1.viajar(1200);        
        
        c2.viajar(500);
        c2.viajar(200);
        
        c3.viajar(100);
        c3.viajar(45);
        c3.viajar(700);
        c3.viajar(200);
        
        Reparacion r1 = new Reparacion(c1, "Pinchazo", LocalDate.of(2021, 6, 12));
        Reparacion r2 = new Reparacion(c2, "Pérdida líquido refrigerante", LocalDate.of(2021, 7, 15));
        Reparacion r3 = new Reparacion(c3, "Pérdida de potencia", LocalDate.of(2021, 9, 22));
        
        taller.addMecanico(m1);
        taller.addMecanico(m2);
        taller.addMecanico(m3);
        
        
        taller.addReparacion(r1);
        taller.addReparacion(r2);
        taller.addReparacion(r3);
        
        r1.reparar("Parcheo", LocalDate.of(2021, Month.JUNE, 15), m1, 15);
        r2.reparar("Cambio de filtro de aceite", LocalDate.of(2021, Month.JULY, 21), m2, 587.55);
        r3.reparar("Cambio del sistema de turbo", LocalDate.of(2021, Month.SEPTEMBER, 30), m3, 2301.50);
        
        System.out.println("Importe reparaciones: " + taller.getImporteReparaciones());
        System.out.println("Coste salarios: " + taller.getSalarioTrabajadores());
        taller.incrementarSalario(3);
        System.out.println("Coste salarios actual: " + taller.getSalarioTrabajadores());
        
        
    }
    
    public static void testViajar(Coche c){
        c.viajar(99999);
    }
    
}
